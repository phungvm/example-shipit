// shipitfile.js
module.exports = shipit => {
  // Load shipit-deploy tasks
  require('shipit-deploy')(shipit)

  shipit.initConfig({
    default: {
      deployTo: '~/example-shipit',
      repositoryUrl: 'https://gitlab.com/phungvm/example-shipit.git',
    },
    development: {
      servers: 'root@139.180.214.57',
      branch: 'master',
    },
  })

  shipit.on('deployed', () => {
    const processName = 'app';
    const env = shipit.environment;

    let cmd = `
      cd ${shipit.releasePath} && 
      npm install &&
      npm run build &&
      pm2 delete app &&
      pm2 start npm --name "app" -- start
    `

    shipit.remote(cmd);
  })
}
